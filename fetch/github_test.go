package fetch

import (
	"errors"
	"github.com/golang/mock/gomock"
	"github.com/google/go-github/v36/github"
	"github.com/stretchr/testify/assert"
	"gitlab.com/summarizer/summarizer-cli/entity"
	mock_main "gitlab.com/summarizer/summarizer-cli/mocks"
	"testing"
	"time"
)

func TestGetStarredItems(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	want := []*entity.Item{
		{
			ID: "gh-123",
		},
	}

	getStarred := mock_main.NewMockGithubConnector(ctrl)

	gh := NewGithub(getStarred)

	const user = "dominikstraessle"
	getStarred.EXPECT().
		ListStarred(gomock.Any(), user, gomock.Any()).
		Return(
			[]*github.StarredRepository{
				{
					Repository: &github.Repository{
						ID:              github.Int64(123),
						Name:            github.String(""),
						FullName:        github.String(""),
						Description:     github.String(""),
						Homepage:        github.String(""),
						HTMLURL:         github.String(""),
						Language:        github.String(""),
						StargazersCount: github.Int(0),
					},
					StarredAt: &github.Timestamp{Time: time.Now()},
				},
			},
			&github.Response{NextPage: 0},
			nil)

	got, err := gh.GetItems(user)
	assert.NoError(t, err)
	if assert.Len(t, got, 1) {
		assert.Equal(t, want[0].ID, got[0].ID)
	}
}

func TestGetStarredItemsError(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	getStarred := mock_main.NewMockGithubConnector(ctrl)

	gh := NewGithub(getStarred)

	const user = "dominikstraessle"
	getStarred.EXPECT().
		ListStarred(gomock.Any(), user, gomock.Any()).
		Return(nil, nil, errors.New("internal error"))

	got, err := gh.GetItems(user)
	assert.Error(t, err)
	assert.Nil(t, got)
}
