package fetch

import (
	"encoding/json"
	"fmt"
	"gitlab.com/summarizer/summarizer-cli/entity"
	"io"
	"os"
)

type BookmarksFetcher struct{}

func (f *BookmarksFetcher) GetItems(userOrFile string) ([]*entity.Item, error) {
	file, err := os.Open(userOrFile)
	if err != nil {
		return nil, err
	}
	return f.parseFile(file)
}

func NewBookmarksFetcher() *BookmarksFetcher {
	return &BookmarksFetcher{}
}

func (f *BookmarksFetcher) parseFile(file io.Reader) ([]*entity.Item, error) {
	input, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var bookmarkEntry *entity.BookmarkEntry
	err = json.Unmarshal(input, &bookmarkEntry)
	if err != nil {
		return nil, err
	}

	items := bookmarkToItem(bookmarkEntry)

	return items, nil
}

func bookmarkToItem(bookmarkEntry *entity.BookmarkEntry) []*entity.Item {
	var newItems []*entity.Item

	if bookmarkEntry.TypeCode == 1 && bookmarkEntry.URI != "" {
		newItems = append(newItems, &entity.Item{
			ID:          fmt.Sprintf("bm-%d", bookmarkEntry.ID),
			Name:        bookmarkEntry.Title,
			Description: bookmarkEntry.Title,
			Type:        entity.Bookmark,
			Homepage:    bookmarkEntry.URI,
			HtmlUrl:     bookmarkEntry.URI,
		})
	}
	for _, child := range bookmarkEntry.Children {
		newItems = append(newItems, bookmarkToItem(child)...)
	}
	return newItems
}
