package fetch

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/summarizer/summarizer-cli/entity"
	"io"
	"strings"
	"testing"
	"time"
)

func TestHackerNewsToItem(t *testing.T) {
	tests := []struct {
		name    string
		article string
		want    *entity.Item
	}{
		{
			name: "article to item",
			article: `
I accidentally built a spying app
https://withblue.ink/2020/09/24/that-time-i-accidentally-built-a-spying-app.html
https://news.ycombinator.com/item?id=25310316`,
			want: &entity.Item{
				ID:          "hn-25310316",
				StarredAt:   time.Time{},
				Name:        "I accidentally built a spying app",
				FullName:    "I accidentally built a spying app",
				Description: "I accidentally built a spying app",
				HtmlUrl:     "https://news.ycombinator.com/item?id=25310316",
				Stars:       0,
				Language:    "",
				Homepage:    "https://withblue.ink/2020/09/24/that-time-i-accidentally-built-a-spying-app.html",
				Type:        entity.HackerNews,
			},
		},
		{
			name: "article to item",
			article: `I accidentally built a spying app
https://withblue.ink/2020/09/24/that-time-i-accidentally-built-a-spying-app.html
https://news.ycombinator.com/item?id=25310316`,
			want: &entity.Item{
				ID:          "hn-25310316",
				StarredAt:   time.Time{},
				Name:        "I accidentally built a spying app",
				FullName:    "I accidentally built a spying app",
				Description: "I accidentally built a spying app",
				HtmlUrl:     "https://news.ycombinator.com/item?id=25310316",
				Stars:       0,
				Language:    "",
				Homepage:    "https://withblue.ink/2020/09/24/that-time-i-accidentally-built-a-spying-app.html",
				Type:        entity.HackerNews,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := hackerNewsToItem(tt.article)
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestHackerNewsFetcher_ParseFile(t *testing.T) {
	tests := []struct {
		name    string
		file    io.Reader
		want    []*entity.Item
		wantErr bool
	}{
		{
			name: "parse file",
			file: strings.NewReader(`Show HN: Personal Management System
https://github.com/Volmarg/personal-management-system
https://news.ycombinator.com/item?id=20670039

Paged Out – a new experimental magazine about programming
https://pagedout.institute
https://news.ycombinator.com/item?id=20670432
`),
			want: []*entity.Item{
				hackerNewsToItem(`Show HN: Personal Management System
https://github.com/Volmarg/personal-management-system
https://news.ycombinator.com/item?id=20670039`),
				hackerNewsToItem(`Paged Out – a new experimental magazine about programming
https://pagedout.institute
https://news.ycombinator.com/item?id=20670432`),
			},
			wantErr: false,
		},
		{
			name: "parse invalid file",
			file: strings.NewReader(`Show HN: Personal Management System
https://news.ycombinator.com/item?id=20670039

`),
			want:    nil,
			wantErr: true,
		},
		{
			name: "parse invalid file",
			file: strings.NewReader(`

d

`),
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := &HackerNewsFetcher{}
			got, err := f.parseFile(tt.file)

			if (err != nil) != tt.wantErr {
				t.Errorf("parseFile() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			assert.Equal(t, tt.want, got)
		})
	}
}
