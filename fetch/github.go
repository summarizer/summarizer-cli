package fetch

import (
	"context"
	"github.com/google/go-github/v36/github"
	"gitlab.com/summarizer/summarizer-cli/entity"
)

type GithubConnector interface {
	ListStarred(ctx context.Context, user string, opts *github.ActivityListStarredOptions) ([]*github.StarredRepository, *github.Response, error)
}

type Fetcher interface {
	GetItems(userOrFile string) ([]*entity.Item, error)
}

type Github struct {
	client GithubConnector
}

func NewGithub(client GithubConnector) *Github {
	return &Github{
		client: client,
	}
}

func (g *Github) GetItems(user string) ([]*entity.Item, error) {
	opt := &github.ActivityListStarredOptions{
		ListOptions: github.ListOptions{PerPage: 100},
	}
	// get all pages of results
	var allItems []*entity.Item
	for {
		repos, resp, err := g.client.ListStarred(context.Background(), user, opt)
		if err != nil {
			return nil, err
		}
		for _, repo := range repos {
			allItems = append(allItems, entity.GithubToItem(repo))
		}
		if resp.NextPage == 0 {
			break
		}
		opt.Page = resp.NextPage
	}
	return allItems, nil
}
