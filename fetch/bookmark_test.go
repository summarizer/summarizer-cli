package fetch

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/summarizer/summarizer-cli/entity"
	"io"
	"os"
	"runtime"
	"strings"
	"testing"
)

func AbsPath(path string) string {
	const currentFilePath = "fetch/bookmark_test.go"
	_, abs, _, _ := runtime.Caller(0)

	file := strings.TrimSuffix(abs, currentFilePath)
	file += path
	return file
}

func TestBookmarksFetcher_parseFile(t *testing.T) {
	file, err := os.Open(AbsPath("data/bookmarks.json"))
	assert.NoError(t, err)
	tests := []struct {
		name    string
		file    io.Reader
		want    *entity.Item
		wantErr bool
	}{
		{
			name: "paser bookmark file",
			file: file,
			want: &entity.Item{
				ID:          "bm-29",
				Type:        entity.Bookmark,
				Homepage:    "https://nomadlist.com/",
				Name:        "Nomad List - Best Cities to Live and Work Remotely for Digital Nomads",
				HtmlUrl:     "https://nomadlist.com/",
				Description: "Nomad List - Best Cities to Live and Work Remotely for Digital Nomads",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := &BookmarksFetcher{}
			got, err := f.parseFile(tt.file)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseFile() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			for _, item := range got {
				if item.ID == "bm-29" {
					assert.Equal(t, tt.want, item)
					break
				}
			}
		})
	}
}
