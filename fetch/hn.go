package fetch

import (
	"bufio"
	"errors"
	"fmt"
	"gitlab.com/summarizer/summarizer-cli/entity"
	"io"
	"os"
	"strings"
	"time"
)

type HackerNewsFetcher struct{}

func (f *HackerNewsFetcher) GetItems(userOrFile string) ([]*entity.Item, error) {
	file, err := os.Open(userOrFile)
	if err != nil {
		return nil, err
	}
	return f.parseFile(file)
}

func NewHackerNewsFetcher() *HackerNewsFetcher {
	return &HackerNewsFetcher{}
}

func splitFunc(data []byte, atEOF bool) (advance int, token []byte, err error) {

	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}

	if i := strings.Index(string(data), "\n\n"); i >= 0 {
		return i + 1, data[0:i], nil
	}

	if atEOF {
		return len(data), data, nil
	}

	return
}

func hackerNewsToItem(article string) *entity.Item {
	trimmed := strings.TrimPrefix(article, "\n")
	lines := strings.Split(trimmed, "\n")

	id := strings.TrimPrefix(lines[2], "https://news.ycombinator.com/item?id=")

	return &entity.Item{
		ID:          fmt.Sprintf("hn-%s", id),
		StarredAt:   time.Time{},
		Name:        lines[0],
		FullName:    lines[0],
		Description: lines[0],
		HtmlUrl:     lines[2],
		Stars:       0,
		Language:    "",
		Homepage:    lines[1],
		Type:        entity.HackerNews,
	}
}

func (f *HackerNewsFetcher) parseFile(file io.Reader) ([]*entity.Item, error) {
	scanner := bufio.NewScanner(file)

	scanner.Split(splitFunc)

	var items []*entity.Item

	for scanner.Scan() {
		text := scanner.Text()
		if text != "" && strings.Count(text, "\n") >= 2 {
			items = append(items, hackerNewsToItem(text))
		} else {
			return nil, errors.New("invalid item")
		}
	}

	err := scanner.Err()
	if err != nil {
		return nil, err
	}

	return items, nil
}
