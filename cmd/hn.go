package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/summarizer/summarizer-cli/fetch"
)

// hnCmd represents the hn command
var hnCmd = &cobra.Command{
	Use:     "hn",
	Short:   "Parse a file with saved HN stories and index them into a linkding instance",
	Long:    `Parse a file with saved HN stories and index them into a linkding instance`,
	Example: "summarizer-cli hn --host=http://localhost:7700 --key=secret data/hn.txt ",
	Args:    cobra.MinimumNArgs(1),
	Run:     indexHN,
}

func indexHN(cmd *cobra.Command, args []string) {
	items, err := fetch.NewHackerNewsFetcher().GetItems(args[0])
	if err != nil {
		cmd.PrintErrln("error while parsing the file")
		return
	}
	indexToLinkding(cmd, items)
}

func init() {
	rootCmd.AddCommand(hnCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	//hnCmd.PersistentFlags().StringVar(&filePath, "file", "", "The .txt file with the saved stories")
	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// hnCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
