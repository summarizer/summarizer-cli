package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/summarizer/summarizer-cli/fetch"
)

// hnCmd represents the hn command
var bmCmd = &cobra.Command{
	Use:     "bm",
	Short:   "Parse a firefox bookmark json file and index them into a linkding instance",
	Long:    `Parse a firefox bookmark json file and index them into a linkding instance`,
	Example: "summarizer-cli bm --host=http://localhost:7700 --key=secret data/bookmarks.json ",
	Args:    cobra.MinimumNArgs(1),
	Run:     indexBM,
}

func indexBM(cmd *cobra.Command, args []string) {
	items, err := fetch.NewBookmarksFetcher().GetItems(args[0])
	if err != nil {
		cmd.PrintErrln("error while parsing the file")
		return
	}
	indexToLinkding(cmd, items)
}

func init() {
	rootCmd.AddCommand(bmCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	//hnCmd.PersistentFlags().StringVar(&filePath, "file", "", "The .txt file with the saved stories")
	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// hnCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
