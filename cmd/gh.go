package cmd

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/google/go-github/v36/github"
	"github.com/spf13/cobra"
	"gitlab.com/summarizer/summarizer-cli/entity"
	"gitlab.com/summarizer/summarizer-cli/fetch"
	"io/ioutil"
	"net/http"
	"time"
)

// ghCmd represents the gh command
var ghCmd = &cobra.Command{
	Use:     "gh",
	Short:   "Fetch starred repos of a user and index them into linkding",
	Long:    `Fetch starred repos of a user and index them into linkding`,
	Example: "summarizer-cli gh --host=http://localhost:7700 --key=secret dominikstraessle",
	Args:    cobra.MinimumNArgs(1),
	Run:     indexGH,
}

func indexGH(cmd *cobra.Command, args []string) {
	ghConnector := github.NewClient(nil)

	items, err := fetch.NewGithub(ghConnector.Activity).GetItems(args[0])
	if err != nil {
		cmd.PrintErrln("error while fetching the starred repos")
		return
	}

	indexToLinkding(cmd, items)
}

func indexToLinkding(cmd *cobra.Command, items []*entity.Item) {
	client := &http.Client{
		Timeout: 15 * time.Second,
	}

	for _, item := range items {
		linkdingRequest := item.ToLinkdingRequests()

		body, err := json.Marshal(linkdingRequest)
		if err != nil {
			cmd.PrintErrf("skipped: error while creating the body for %v\n", linkdingRequest)
			continue
		}
		req, err := http.NewRequest(http.MethodPost, fmt.Sprintf("%s/api/bookmarks/", linkdingHost), bytes.NewReader(body))
		if err != nil {
			cmd.PrintErrf("skipped: error while creating the request for %v\n", linkdingRequest)
			continue
		}
		req.Header.Set("Authorization", fmt.Sprintf("Token %s", linkdingKey))
		req.Header.Set("Content-Type", "application/json")

		resp, err := client.Do(req)
		if err != nil {
			cmd.PrintErrf("skipped: error while doing the request for %v\n", linkdingRequest)
			continue
		}
		if resp.StatusCode != http.StatusCreated {
			cmd.PrintErrf("skipped: error while doing the request for %v\n", linkdingRequest)
			body, _ = ioutil.ReadAll(resp.Body)
			cmd.PrintErrf("status: %d error: %v", resp.StatusCode, body)
		}
		_ = resp.Body.Close()
	}
}

func init() {
	rootCmd.AddCommand(ghCmd)
}
