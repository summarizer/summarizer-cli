package entity

import (
	"github.com/google/go-github/v36/github"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestGithubToItem(t *testing.T) {
	date := time.Date(2000, time.April, 3, 0, 0, 0, 0, time.UTC)
	tests := []struct {
		name  string
		repos *github.StarredRepository
		want  *Item
	}{
		{
			name: "github starred repo to item",
			repos: &github.StarredRepository{
				StarredAt: &github.Timestamp{Time: date},
				Repository: &github.Repository{
					ID:              github.Int64(int64(123)),
					Name:            github.String("name"),
					FullName:        github.String("full_name"),
					Description:     github.String("description"),
					Homepage:        github.String("homepage"),
					HTMLURL:         github.String("html_url"),
					Language:        nil,
					StargazersCount: github.Int(321),
				},
			},
			want: &Item{
				ID:          "gh-123",
				StarredAt:   date,
				Name:        "name",
				FullName:    "full_name",
				Description: "description",
				Homepage:    "homepage",
				HtmlUrl:     "html_url",
				Language:    "",
				Stars:       321,
				Type:        Github,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := GithubToItem(tt.repos)
			assert.Equal(t, *tt.want, *got)
		})
	}
}
