package entity

import (
	"fmt"
	"github.com/google/go-github/v36/github"
	"time"
)

type ItemType string

const (
	Github     = "gh"
	HackerNews = "hn"
	Bookmark   = "bm"
)

type LinkdingRequest struct {
	Url         string   `json:"url"`
	Title       string   `json:"title"`
	Description string   `json:"description"`
	IsArchived  bool     `json:"is_archived"`
	Unread      bool     `json:"unread"`
	Shared      bool     `json:"shared"`
	TagNames    []string `json:"tag_names"`
}

type Item struct {
	ID          string    `json:"id,omitempty"`
	StarredAt   time.Time `json:"starred_at,omitempty"`
	Name        string    `json:"name,omitempty"`
	FullName    string    `json:"full_name,omitempty"`
	Description string    `json:"description,omitempty"`
	HtmlUrl     string    `json:"html_url,omitempty"`
	Stars       int       `json:"stars,omitempty"`
	Language    string    `json:"language,omitempty"`
	Homepage    string    `json:"homepage,omitempty"`
	Type        ItemType  `json:"type,omitempty"`
}

func getValue(pointer *string) string {
	if pointer == nil {
		return ""
	}
	return *pointer
}

func GithubToItem(repo *github.StarredRepository) *Item {
	return &Item{
		ID:          fmt.Sprintf("gh-%d", *repo.Repository.ID),
		StarredAt:   repo.StarredAt.Time,
		Name:        getValue(repo.Repository.Name),
		FullName:    getValue(repo.Repository.FullName),
		Description: getValue(repo.Repository.Description),
		HtmlUrl:     getValue(repo.Repository.HTMLURL),
		Stars:       *repo.Repository.StargazersCount,
		Homepage:    getValue(repo.Repository.Homepage),
		Language:    getValue(repo.Repository.Language),
		Type:        Github,
	}
}

func (i *Item) ToLinkdingRequests() *LinkdingRequest {
	var tagNames []string
	tagNames = append(tagNames, fmt.Sprintf("%s", i.Type))
	if i.Language != "" {
		tagNames = append(tagNames, fmt.Sprintf("%s", i.Language))
	}

	e := &LinkdingRequest{
		Url:         i.HtmlUrl,
		Title:       i.Name,
		Description: i.Description,
		IsArchived:  false,
		Unread:      true,
		Shared:      false,
		TagNames:    tagNames,
	}

	return e
}
