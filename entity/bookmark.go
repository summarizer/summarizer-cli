package entity

type BookmarkEntry struct {
	GUID         string           `json:"guid"`
	Title        string           `json:"title"`
	Index        int              `json:"index"`
	DateAdded    int64            `json:"dateAdded"`
	LastModified int64            `json:"lastModified"`
	ID           int              `json:"id"`
	TypeCode     int              `json:"typeCode"`
	Type         string           `json:"type"`
	Root         string           `json:"root"`
	Children     []*BookmarkEntry `json:"children"`
	URI          string           `json:"uri,omitempty"`
	Iconuri      string           `json:"iconuri,omitempty"`
}
