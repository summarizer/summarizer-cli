# summarizer-cli
This is a cli to populate your [linkding instance](https://github.com/sissbruecker/linkding) with your own bookmarks, github stars and exported hacker news stories.
It supports different sources and item types.

## Supported Sources
- Github Stars
    - All starred projects of a specific user
    - Consumes the Github Rest API
- Hacker News Stories
    - Hacker News Stories exported with the [Materialistic](https://github.com/hidroh/materialistic) app.
    - Checkout the format of the example .txt file at [data/hn.txt](./data/hn.txt)
- Firefox Bookmarks
    - All Firefox Bookmarks exported as a .json file
    - Checkout the format of the example .json file at [data/bookmarks.json](./data/bookmarks.json)
    
## Usage

Download the binary from the latest job artifact or build it by yourself.

Then run the help command for more information
```shell
./summarizer-cli help
```

To check out the commands and examples for a specific sources run
```shell
./summarizer-cli hn help # for Hacker News
./summarizer-cli bm help # for bookmarks
./summarizer-cli gh help # for Github Stars
```
